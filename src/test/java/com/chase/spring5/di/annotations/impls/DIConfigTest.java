package com.chase.spring5.di.annotations.impls;

import static org.junit.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/*
 * Seems like only spring boot supports annotations for configurint testng,
 * and we don't want spring boot now, we want simple udnerstandable examples mmkay.
 */
@ContextConfiguration(/*locations = "/config-02.xml",*/ classes = {DiLidlConfiguration.class} )
public class DIConfigTest extends AbstractTestNGSpringContextTests {
	
	@Autowired
    private ApplicationContext appContext;
	
	@Test
	public void diWokrs()	{
		
		assertNotNull(appContext);
		
		LidlShoppingListService service = appContext.getBean(LidlShoppingListService.class);
		
		System.out.println(service);
	}
}
