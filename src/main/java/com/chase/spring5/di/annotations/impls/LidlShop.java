package com.chase.spring5.di.annotations.impls;

import org.springframework.stereotype.Component;

import com.chase.spring5.di.annotations.abstracts.AbstractShop;

/*
 * Not being paid to advertise, but I tend to go to lidl a lot...so.... here it is.
 */
@Component("lidl")
public class LidlShop extends AbstractShop {
	
	@Override
	public String getShopAddress() {
		return shopAddress;
	}

	@Override
	public String getBranding() {
		return "Lidl";
	}

	@Override
	public Boolean isRetail() {
		return true;
	}

	@Override
	public Boolean isDiscount() {
		return false;
	}

	@Override
	public Double getDiscountInPercentage() {
		return null;
	}

	@Override
	public String toString() {
		return "LidlShop [shopAddress=" + shopAddress + ", branding=" + branding + ", retail=" + retail + ", discount="
				+ discount + ", discountInPercentage=" + discountInPercentage + "]";
	}
}
