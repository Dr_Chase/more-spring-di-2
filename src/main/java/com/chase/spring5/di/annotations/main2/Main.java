package com.chase.spring5.di.annotations.main2;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.chase.spring5.di.annotations.impls.LidlShoppingList;
import com.chase.spring5.di.annotations.impls.LidlShoppingListService;
import com.chase.spring5.di.annotations.interfaces.ShoppingList;
import com.chase.spring5.di.annotations.interfaces.ShoppingListService;

public class Main {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		ApplicationContext context = new AnnotationConfigApplicationContext("com.chase.spring5.di.annotations.impls");
		

		ShoppingList sList = context.getBean(LidlShoppingList.class);
		
		System.out.println(sList);
		
		ShoppingListService sls = context.getBean(LidlShoppingListService.class);
		
		List<String> shopList = sls.getShopNames();
		
		for(String shopName : shopList) {
			
			System.out.println(shopName);
		}
		
		
	}

}
