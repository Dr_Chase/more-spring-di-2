package com.chase.spring5.di.annotations.impls;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.chase.spring5.di.annotations.abstracts.AbstractShoppingListService;
import com.chase.spring5.di.annotations.interfaces.ShoppingList;

/*
 * Attempting to demonstrate constructor injection.
 */
@Component("lidl shopping list service")
public class LidlShoppingListService extends AbstractShoppingListService {

	
	public LidlShoppingListService(@Autowired @Qualifier("lidllist") ShoppingList shoppingList)	{
		
		this.shoppingList = shoppingList;
	}
	
	@Override
	public List<String> getShopNames()	{
		
		List<String> list = new ArrayList<>();
		list.add("Lidl");
		
		return list;
	}
}
