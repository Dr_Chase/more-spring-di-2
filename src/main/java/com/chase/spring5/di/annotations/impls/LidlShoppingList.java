package com.chase.spring5.di.annotations.impls;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.chase.spring5.di.annotations.abstracts.AbstractShoppingList;
import com.chase.spring5.di.annotations.interfaces.Shop;

@Component("lidllist")
public class LidlShoppingList extends AbstractShoppingList {
	
	@Autowired
	@Qualifier("lidl")
	private Shop lidlShop;

	public String getNameOfList() {
		
		return "Lidl list";
	}
	
	@Override
	public List<Shop> getShopsBoughtFrom()	{
		
		List<Shop> output = new ArrayList<Shop>();
		output.add(lidlShop);
		return output;
	}

	@Override
	public String toString() {
		return "LidlShoppingList [lidlShop=" + lidlShop + ", shoppingList=" + shoppingList + "]";
	}

	
}
