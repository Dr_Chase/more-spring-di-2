package com.chase.spring5.di.annotations.impls;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan // or @ComponentScan(basePackages = "com.chase.spring5.di.annotations.impls")
public class DiLidlConfiguration {

	
}
